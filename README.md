# Spring Boot JWT Authentication example with Spring Security & Spring Data JPA
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-spring-security-jwt-authentication.git`.
2. Go inside folder: `cd spring-boot-spring-security-jwt-authentication`.
3. Run the application: `mvn clean spring-boot:run`

Tables that we define in models package will be automatically generated in Database.
If you check PostgreSQL for example, you can see things like this:
```shell script
jwtAuth=# \d users;
                                     Table "public.users"
  Column  |          Type          | Collation | Nullable |              Default              
----------+------------------------+-----------+----------+-----------------------------------
 id       | bigint                 |           | not null | nextval('users_id_seq'::regclass)
 email    | character varying(50)  |           |          | 
 password | character varying(120) |           |          | 
 username | character varying(20)  |           |          | 
Indexes:
    "users_pkey" PRIMARY KEY, btree (id)
    "uk6dotkott2kjsp8vw4d0m25fb7" UNIQUE CONSTRAINT, btree (email)
    "ukr43af9ap4edm43mmtq01oddj6" UNIQUE CONSTRAINT, btree (username)
Referenced by:
    TABLE "user_roles" CONSTRAINT "fkhfh9dx7w3ubf1co1vdev94g3f" FOREIGN KEY (user_id) REFERENCES users(id)

jwtAuth=# \d roles;
                                   Table "public.roles"
 Column |         Type          | Collation | Nullable |              Default              
--------+-----------------------+-----------+----------+-----------------------------------
 id     | integer               |           | not null | nextval('roles_id_seq'::regclass)
 name   | character varying(20) |           |          | 
Indexes:
    "roles_pkey" PRIMARY KEY, btree (id)
Referenced by:
    TABLE "user_roles" CONSTRAINT "fkh8ciramu9cc9q3qcqiv4ue8a6" FOREIGN KEY (role_id) REFERENCES roles(id)

jwtAuth=# \d user_roles
             Table "public.user_roles"
 Column  |  Type   | Collation | Nullable | Default 
---------+---------+-----------+----------+---------
 user_id | bigint  |           | not null | 
 role_id | integer |           | not null | 
Indexes:
    "user_roles_pkey" PRIMARY KEY, btree (user_id, role_id)
Foreign-key constraints:
    "fkh8ciramu9cc9q3qcqiv4ue8a6" FOREIGN KEY (role_id) REFERENCES roles(id)
    "fkhfh9dx7w3ubf1co1vdev94g3f" FOREIGN KEY (user_id) REFERENCES users(id)
```

We also need to add some rows into roles table before assigning any role to User.
Run following SQL insert statements:
```sql
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
```
Then check the tables:
```sql
jwtAuth=# SELECT * FROM roles;
 id |      name      
----+----------------
  1 | ROLE_USER
  2 | ROLE_MODERATOR
  3 | ROLE_ADMIN
(3 rows)

jwtAuth=# SELECT * FROM user_roles;
 user_id | role_id 
---------+---------
       1 |       2
       1 |       1
       2 |       1
       3 |       1
       4 |       3
(5 rows)
```

Register some users with `/signup` API:

* kakashi with `ROLE_ADMIN`
* naruto with `ROLE_MODERATOR` and `ROLE_USER`
* sasuke & sakura with `ROLE_USER`

![Kakashi](img/kakashi.png "Role Admin")

![Naruto](img/naruto.png "Role Moderator & Role User")

![Sakura](img/sakura.png "Role User")

Our tables after signup could look like this.
```sql
jwtAuth=# SELECT * FROM users;
 id |               email               |                           password                           | username 
----+-----------------------------------+--------------------------------------------------------------+----------
  1 | uzumaki_naruto@konohagakure.co.jp | $2a$10$pp2t91y2dZfCrK1wiPTSfuoaGAR1rdjVqJk4K73G/HzJPnpDCq0Zu | naruto
  2 | uchiha_sasuke@konohagakure.co.jp  | $2a$10$TOdwBD1bZ5h9VVbCOg18merL.EMN8IDMLcdjNGdx8lZrRlWaBLglu | sasuke
  3 | haruno_sakura@konohagakure.co.jp  | $2a$10$tswMfSI5Cx.VYs31mj2gteO0j9CjpUZ8uGz/OvOFAIq/IUoGxOKVm | sakura
  4 | hatake_kakashi@konohagakure.co.jp | $2a$10$kHTa523qoBAsRPsYeupJ9eG9i6m4IXl3xkYItfs/XnHt0XznphXQG | kakashi
(4 rows)


jwtAuth=# SELECT * FROM roles;
 id |      name      
----+----------------
  1 | ROLE_USER
  2 | ROLE_MODERATOR
  3 | ROLE_ADMIN
(3 rows)

jwtAuth=# SELECT * FROM user_roles;
 user_id | role_id 
---------+---------
       1 |       2
       1 |       1
       2 |       1
       3 |       1
       4 |       3
(5 rows)

```

Access public resource: `GET /api/test/all`

![Access Public Resource](img/access-public.png "Access Public Resource")

Access protected resource: `GET /api/test/user`

![Access protected resource](img/401.png "Access protected resource")

Login an account: `POST /api/auth/signin`

![Login an account](img/signin-user.png "Login an account" )

Access **ROLE_USER** resource: `GET /api/test/user`

![Access ROLE_USER resource](img/access-user.png "Access ROLE_USER resource")

Access **ROLE_MODERATOR** resource: `GET /api/test/mod`

![Access ROLE_MODERATOR resource](img/access-mod.png "Access ROLE_MODERATOR resource")

Access **ROLE_ADMIN** resource: `GET /api/test/admin`

![Access ROLE_ADMIN resource](img/access-admin.png "Access ROLE_ADMIN resource")
