package com.hendisantika.springbootspringsecurityjwtauthentication.model;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-spring-security-jwt-authentication
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/20
 * Time: 07.23
 */
public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}