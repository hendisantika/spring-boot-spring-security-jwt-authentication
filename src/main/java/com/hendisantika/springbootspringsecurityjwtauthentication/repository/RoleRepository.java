package com.hendisantika.springbootspringsecurityjwtauthentication.repository;

import com.hendisantika.springbootspringsecurityjwtauthentication.model.ERole;
import com.hendisantika.springbootspringsecurityjwtauthentication.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-spring-security-jwt-authentication
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/20
 * Time: 07.29
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
